#!/bin/bash

#example to work with ini file: version=$(awk -F "=" '/database_version/ {print $2}' parameters.ini)
#https://askubuntu.com/questions/743493/best-way-to-read-a-config-file-in-bash
#params: --task={backup,restore} --db={postgresql,mysql,sqlite} --ini={path_to_ini_file}
