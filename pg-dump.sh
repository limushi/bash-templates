#!/bin/bash

logger "Postgres backup start"
printf "Subject: Check restore\n\nRock 'n' roll!" | msmtp -v email@example.com

dt=`date +%Y%m%d%H%M`
#echo $dt
pg_dump -v -h localhost --format d --file /backup/pgdumps/$dt --schema ""public"" -U postgres dwh -j 1
#pg_dump -v -h localhost --format d --file /backup/pgdumps/$dt -U postgres dwh -j 3

logger "Postgres backup end"

grep "Postgres" /var/log/syslog
