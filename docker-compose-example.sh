#!/bin/sh

taskid="task1"

run_command() {
    /usr/local/bin/docker-compose exec -T backend python scripts/upload.py --now true
}

test_command() {
    sleep 10
}

run_command_pid() {

    if [ -f /tmp/$taskid.pid ]
        then exit
    else
        sleep 5
        touch /tmp/$taskid.pid
        sleep 5
        # run command
    fi
}

logger "Start $taskid"

run_command_pid && logger "End $taskid"

if [ -f /tmp/$taskid.pid ]; then /bin/rm -f /tmp/$taskid.pid; fi
